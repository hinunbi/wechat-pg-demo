package com.shb.process;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.http.common.HttpMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ClientAddressExtractor implements Processor {
  
  private static final Logger logger = LoggerFactory.getLogger(ClientAddressExtractor.class);
  @Override
  public void process(Exchange exchange) throws Exception {

    Message in = exchange.getIn();

    HttpMessage httpMessage = in.getBody(HttpMessage.class);
    String remoteAddr = httpMessage.getRequest().getRemoteAddr();
    in.setHeader("FUSE_REMOTE_ADDR", remoteAddr);
  }
}
